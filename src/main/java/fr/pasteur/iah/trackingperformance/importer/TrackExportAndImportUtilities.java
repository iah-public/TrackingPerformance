/*-
 * #%L
 * Image Analysis Hub support for Life Scientists.
 * %%
 * Copyright (C) 2021 IAH developers.
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the IAH / C2RT / Institut Pasteur nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.iah.trackingperformance.importer;


import java.io.File;
import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import fr.pasteur.iah.trackingperformance.measure.Detection;
import fr.pasteur.iah.trackingperformance.measure.TrackSegment;

/**
 * Track import/export utilities.
 *  Input and output files are in the .xml format used for the ISBI'2012 Particle Tracking Challenge
 *  Tracks are of the type TrackSegment that is used by the TrackManager plugin
 *  
 * @version February 3, 2012
 * 
 * @author Nicolas Chenouard
 * @author Fabrice de Chaumont
 * @author Ihor Smal
 */

public class TrackExportAndImportUtilities {

	/**
	 * Load TrackSegment objects from a .xml file.
	 * @param inputFile an .xml file containing track information in the format used for the ISBI'2012 Particle tracking challenge
	 * @return a list of TrackSegment objects that corresponds to the loaded tracks
	 * 
	 * */
	public static ArrayList<TrackSegment> importTracksFile (final File inputFile) throws IllegalArgumentException
	{
		final ArrayList<TrackSegment> trackArrayList = new ArrayList<TrackSegment>();
		final Document document = XMLUtil.loadDocument( inputFile );
		final Element root = XMLUtil.getRootElement( document );
		if ( root == null )
		{
			throw new IllegalArgumentException( "can't find: <root> tag." );
		}
		final Element trackingSet = XMLUtil.getElements( root , "TrackContestISBI2012" ).get( 0 );

		if ( trackingSet == null )
		{
			throw new IllegalArgumentException( "can't find: <root><TrackContestISBI2012> tag." ) ;		
		}

		final ArrayList<Element> particleElementArrayList = XMLUtil.getElements( trackingSet , "particle" );

		for ( final Element particleElement : particleElementArrayList )
		{
			final ArrayList<Element> detectionElementArrayList =
					XMLUtil.getElements( particleElement , "detection" );
			final TreeMap<Integer, Detection> detections = new TreeMap<Integer, Detection>();
			for ( final Element detectionElement : detectionElementArrayList )	
			{
				final int t = XMLUtil.getAttributeIntValue( detectionElement, "t" , -1 );
				if ( t < 0 )
					throw new IllegalArgumentException( "invalid t value: " + t ) ;				
				if ( detections.containsKey( Integer.valueOf( t ) ) )
					throw new IllegalArgumentException( "duplicated detection for a single track at time " + t ) ;				
				final double x = XMLUtil.getAttributeDoubleValue( detectionElement, "x" , 0 );
				final double y = XMLUtil.getAttributeDoubleValue( detectionElement, "y" , 0 );
				final double z = XMLUtil.getAttributeDoubleValue( detectionElement, "z" , 0 );		
				final Detection newDetection = new Detection(x, y, z, t);
				newDetection.setDetectionType(Detection.DETECTIONTYPE_REAL_DETECTION);
				detections.put( Integer.valueOf( t ), newDetection );
			}
			// add detections in chronological order and cap gaps with virtual detections
			if (!detections.isEmpty())
			{
				final TrackSegment track = new TrackSegment();
				Detection lastDetection = null;
				for (final Entry<Integer, Detection> e:detections.entrySet())
				{
					final Detection detection = e.getValue();
					if (lastDetection!=null)
					{
						// cap hole with virtual detections
						if (detection.getT()>lastDetection.getT()+1)
						{
							final int lastT = lastDetection.getT();
							final int nextT = detection.getT();
							final double lastX = lastDetection.getX();
							final double lastY = lastDetection.getY();
							final double lastZ = lastDetection.getZ();
							final double nextX = detection.getX();
							final double nextY = detection.getY();
							final double nextZ = detection.getZ();
							final double gapT = 1/((double)nextT-(double)lastT);
							for (int t = lastT+1; t < nextT; t++)
							{
								// linear interpolation
								final Detection interpolatedDetection = new Detection(
										lastX + (t-lastT)*(nextX-lastX)*gapT,
										lastY + (t-lastT)*(nextY-lastY)*gapT,
										lastZ + (t-lastT)*(nextZ-lastZ)*gapT,
										t
										);
//								System.out.println(interpolatedDetection.getX()+" "+interpolatedDetection.getY()+" "+interpolatedDetection.getZ());
								interpolatedDetection.setDetectionType(Detection.DETECTIONTYPE_VIRTUAL_DETECTION);
								track.addDetection(interpolatedDetection);
							}
						}
					}
					track.addDetection(detection);
					lastDetection = detection;
				}
				trackArrayList.add( track );
			}
		}
		
		// check if detection contain NaN position, and trim track if they exists
		// example ( considering only one coordinate )
		// NaN, 1 , 2 ,4 ,12 , NaN, NaN => 1 , 2 , 4 , 12
		// NaN , 1 , Nan , 2 , 3 , NaN => Discarded
		
		for ( final TrackSegment track : new ArrayList<TrackSegment>(trackArrayList) )
		{
			// trim NaN from the beginning of track.
			for ( final Detection detection : new ArrayList<Detection>(track.getDetectionList()) )
			{
				if ( containsNaN( detection ) )
				{
					track.removeDetection( detection );
				}else
				{
					break;
				}
			}
			
			// trim NaN from the end of track.
			for ( int i = track.getDetectionList().size()-1 ; i>=0 ; i-- )
			{
				final Detection detection = track.getDetectionList().get( i );
				if ( containsNaN( detection ) )
				{
					track.removeDetection( detection );
				}else
				{
					break;
				}
			}
			
			if ( track.getDetectionList().size() == 0 )
			{
				trackArrayList.remove( track );
				continue;
			}
						
			// check if a NaN still exists in the remaining track			
			for ( final Detection detection : new ArrayList<Detection>(track.getDetectionList()) )
			{
				if ( containsNaN( detection ) )
				{
					trackArrayList.remove( track );					
					break;
				}
			}
			
			
			
		}
		
//		System.out.println( trackArrayList.size() +" track(s) succesfuly loaded.");
		return trackArrayList;
	}
	
	private static boolean containsNaN( final Detection detection )
	{
		return ( 
				Double.isNaN( detection.getX() ) 
				|| Double.isNaN( detection.getY() )
				|| Double.isNaN( detection.getZ() )				
				);
	}

	/**
	 * Export TrackSegment objects to a .xml file.
	 * @param file output .xml file containing track information in the format used for the ISBI'2012 Particle tracking challenge
	 * @param tracks a list of TrackSegment objects that corresponds to the tracks to save
	 * 
	 * */
	public static void exportTracks (final File file, final ArrayList<TrackSegment> tracks) throws IllegalArgumentException {
		final Document document = XMLUtil.createDocument( true );

		final Element dataSetElement = document.createElement("TrackContestISBI2012");

		XMLUtil.getRootElement( document ).appendChild( dataSetElement );


		for ( final TrackSegment particle: tracks )
		{
			final Element particleElement =  document.createElement("particle");
			dataSetElement.appendChild(particleElement);

			for ( final Detection detection : particle.getDetectionList() )
			{
				final Element detectionElement =  document.createElement("detection");
				particleElement.appendChild( detectionElement );
				XMLUtil.setAttributeDoubleValue( detectionElement , "x" , roundDecimals2(detection.getX()) );
				XMLUtil.setAttributeDoubleValue( detectionElement , "y" , roundDecimals2(detection.getY()) );
				XMLUtil.setAttributeDoubleValue( detectionElement , "z" , roundDecimals2(detection.getZ()) );
				XMLUtil.setAttributeIntValue( detectionElement , "t" , detection.getT() );
			}

		}

		XMLUtil.saveDocument( document , file );
	}

	private static double roundDecimals2(double value) {
		value = value * 1000d;
		value = Math.round(value);
		return value/1000d;       
	}
}
